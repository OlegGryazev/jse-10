package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.ISettingService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Setting;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Logout from Project Manager.";
    }

    @Override
    public void execute() {
        if (terminalService == null || serviceLocator == null) return;
        @NotNull final ISettingService settingService = serviceLocator.getSettingService();
        @Nullable final String currentUserId = settingService.findValueByKey("userId");
        if (currentUserId == null) return;
        settingService.setSetting(new Setting("userId", null));
        terminalService.print("You are logged out.");
    }

}
