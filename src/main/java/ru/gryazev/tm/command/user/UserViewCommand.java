package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;

@NoArgsConstructor
public final class UserViewCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-view";
    }

    @Override
    public String getDescription() {
        return "View user data.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        @Nullable final String currentUserId = serviceLocator.getSettingService().findValueByKey("userId");
        @Nullable final User user = serviceLocator.getUserService().findOne(currentUserId, currentUserId);
        if (user != null) terminalService.print(user.toString());
    }

}
