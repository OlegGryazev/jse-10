package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.error.CrudUpdateException;

import java.io.IOException;

@NoArgsConstructor
public final class UserEditCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user data.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final IUserService userService = serviceLocator.getUserService();
        terminalService.print("[USER EDIT]");
        @Nullable final User userEditData = terminalService.getUserPwdRepeat();
        if (userEditData == null) return;
        @Nullable final String currentUserId = serviceLocator.getSettingService().findValueByKey("userId");

        @Nullable final User currentUser = userService.findOne(currentUserId, currentUserId);
        if (currentUser == null) throw new CrudUpdateException();
        userEditData.setRoleType(currentUser.getRoleType());
        userEditData.setId(currentUser.getId());
        @Nullable final User editedUser = userService.edit(currentUser.getUserId(), userEditData);
        if (editedUser == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
