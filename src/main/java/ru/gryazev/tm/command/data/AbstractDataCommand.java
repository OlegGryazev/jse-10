package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.dto.Domain;
import ru.gryazev.tm.entity.Setting;
import ru.gryazev.tm.enumerated.RoleType;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public Domain prepareDomain() {
        @NotNull final Domain domain = new Domain();
        if (serviceLocator == null || terminalService == null) return domain;
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void loadDataFromDomain(@NotNull final Domain domain) {
        if (serviceLocator == null || domain.getUsers().isEmpty()) return;
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        projectService.removeAll();
        taskService.removeAll();
        userService.removeAll();
        serviceLocator.getSettingService().setSetting(new Setting("userId", null));
        domain.getProjects().forEach(o -> projectService.create(o.getUserId(), o));
        domain.getTasks().forEach(o -> taskService.create(o.getUserId(), o));
        domain.getUsers().forEach(o -> userService.create(o.getUserId(), o));
    }

    @Nullable
    @Override
    public RoleType[] getRoles() {
        return new RoleType[]{RoleType.ADMIN};
    }

}
