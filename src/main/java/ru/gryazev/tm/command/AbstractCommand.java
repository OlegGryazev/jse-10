package ru.gryazev.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.CommandLocator;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.service.ISettingService;
import ru.gryazev.tm.api.service.ITerminalService;
import ru.gryazev.tm.enumerated.RoleType;

import javax.xml.bind.JAXBException;
import java.io.IOException;

@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    @Nullable
    protected ITerminalService terminalService;

    @Nullable
    protected ISettingService settingService;

    @Nullable
    protected CommandLocator commandLocator;

    @Getter
    private boolean allowed = false;

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException, ClassNotFoundException, JAXBException;

    @Nullable
    public RoleType[] getRoles() {
        return null;
    }

    @Nullable
    public String getCurrentUserId() {
        if (settingService == null) return null;
        return settingService.findValueByKey("userId");
    }

}
