package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.api.service.ISettingService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public class ProjectFindByDetails extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-find-details";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List of projects by details or part of details.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final ISettingService settingService = serviceLocator.getSettingService();
        @NotNull final String projectDetails = terminalService.getSearchString();
        @NotNull final List<Project> projects = serviceLocator.getProjectService()
                .findByDetails(getCurrentUserId(),projectDetails);
        if (projects.size() == 0) throw new CrudListEmptyException();
        projects.sort(getComparator());
        for (int i = 0; i < projects.size(); i++)
            terminalService.print((i + 1) + ". " + projects.get(i).getName());
    }

}
