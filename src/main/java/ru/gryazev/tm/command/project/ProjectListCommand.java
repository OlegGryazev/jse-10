package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findByUserId(getCurrentUserId());
        if (projects.size() == 0) throw new CrudListEmptyException();
        projects.sort(getComparator());
        for (int i = 0; i < projects.size(); i++)
            terminalService.print((i + 1) + ". " + projects.get(i).getName());
    }

}
