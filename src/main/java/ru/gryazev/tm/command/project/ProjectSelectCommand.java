package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Setting;

@NoArgsConstructor
public final class ProjectSelectCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Select project. To reset, type \"-1\".";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String userId = getCurrentUserId();
        @Nullable final String projectId = serviceLocator.getProjectService()
                .getProjectId(projectIndex, userId, getComparator());
        serviceLocator.getSettingService().edit(userId, new Setting("current-project", projectId));
    }

}
