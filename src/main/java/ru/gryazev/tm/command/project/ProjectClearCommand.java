package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Clear projects list.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        @Nullable final String userId = serviceLocator.getSettingService().findValueByKey("userId");
        serviceLocator.getProjectService().removeAll(userId);
        serviceLocator.getTaskService().removeAll(userId);
        terminalService.print("[CLEAR OK]");
    }

}
