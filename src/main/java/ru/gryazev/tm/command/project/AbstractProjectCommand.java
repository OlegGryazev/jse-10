package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.comparator.ComparableEntityComparator;

import java.util.Comparator;

@NoArgsConstructor
public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    public Comparator<ComparableEntity> getComparator() {
        if (settingService == null) return ComparableEntityComparator.comparatorCreateMillis;
        @Nullable final String sortType = settingService.findValueByKey("project-sort");
        return ComparableEntityComparator.getComparator(sortType);
    }

}
