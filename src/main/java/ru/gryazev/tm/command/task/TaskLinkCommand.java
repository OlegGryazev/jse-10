package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class TaskLinkCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-link";
    }

    @Override
    public String getDescription() {
        return "Link selected task to project.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        @Nullable final String userId = getCurrentUserId();
        @Nullable final String currentProjectId = getCurrentProjectId();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = serviceLocator.getProjectService()
                .getProjectId(projectIndex, userId, getComparator());
        final int taskIndex = terminalService.getTaskIndex();

        @Nullable final Task linkedTask = serviceLocator.getTaskService()
                .linkTask(userId, currentProjectId, projectId, taskIndex);
        if (linkedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
