package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.comparator.ComparableEntityComparator;
import ru.gryazev.tm.error.CrudNotFoundException;

import java.util.Comparator;

@NoArgsConstructor
public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public Comparator<ComparableEntity> getComparator() {
        if (settingService == null) return ComparableEntityComparator.comparatorCreateMillis;
        @Nullable final String sortType = settingService.findValueByKey("task-sort");
        return ComparableEntityComparator.getComparator(sortType);
    }

    @Nullable
    public String getCurrentProjectId() {
        if (settingService == null) return null;
        return settingService.findValueByKey("current-project");
    }

    @Nullable
    public String getProjectId() {
        if (serviceLocator == null || terminalService == null) return null;
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable String projectId = getCurrentProjectId();
        if (projectId == null) {
            projectId = projectService.getProjectId(terminalService.getProjectIndex(), getCurrentUserId(), getComparator());
        }
        if (projectId == null) throw new CrudNotFoundException();
        return projectId;
    }

}
