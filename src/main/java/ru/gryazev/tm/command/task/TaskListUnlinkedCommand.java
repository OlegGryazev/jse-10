package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

@NoArgsConstructor
public final class TaskListUnlinkedCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-list-unlinked";
    }

    @Override
    public String getDescription() {
        return "Shows all unlinked tasks.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final List<Task> tasks = taskService.listTaskUnlinked(getCurrentUserId());
        if (tasks.size() == 0) throw new CrudListEmptyException();
        tasks.sort(getComparator());
        for (int i = 0; i < tasks.size(); i++)
            terminalService.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
