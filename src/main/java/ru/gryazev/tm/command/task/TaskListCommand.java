package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of selected project.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        @Nullable final String projectId = getCurrentProjectId();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final List<Task> tasks = taskService.listTaskByProject(getCurrentUserId(), projectId);
        if (tasks.size() == 0) throw new CrudListEmptyException();
        tasks.sort(getComparator());
        for (int i = 0; i < tasks.size(); i++)
            terminalService.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
