package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Clear tasks list.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        serviceLocator.getTaskService().removeAll(getCurrentUserId());
        terminalService.print("[CLEAR OK]");
    }

}
