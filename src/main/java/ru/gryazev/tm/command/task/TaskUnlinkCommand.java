package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class TaskUnlinkCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-unlink";
    }

    @Override
    public String getDescription() {
        return "Unlink selected task.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final String userId = getCurrentUserId();
        @Nullable final String currentProjectId = getCurrentProjectId();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String taskId = taskService.getTaskId(currentProjectId, userId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();

        @Nullable final Task unlinkedTask = taskService.unlinkTask(userId, taskService.findOne(userId, taskId));
        if (unlinkedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
