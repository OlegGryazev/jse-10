package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.entity.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    public List<Task> findTasksByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    public List<Task> findUnlinked(@NotNull String userId);

    @Nullable
    public Task findTaskByIndex(@NotNull String projectId, @NotNull String userId, int taskIndex);

    @Nullable
    public Task findTaskByIndex(@NotNull String projectId,
                                @NotNull String userId,
                                int taskIndex,
                                @NotNull Comparator<ComparableEntity> comparator);

    @Nullable
    public Task findUnlinkedTaskByIndex(@NotNull String userId, int taskIndex);

    @NotNull
    public List<Task> findByName(@NotNull String userId, @NotNull String taskName);

    @NotNull
    public List<Task> findByDetails(@NotNull String userId, @NotNull String taskDetails);

}
