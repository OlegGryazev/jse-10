package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.List;
import java.util.Map;

public interface IRepository<T extends AbstractCrudEntity> {

    @NotNull
    public List<T> findAll();

    @Nullable
    public T findOne(@NotNull String userId, @NotNull String id);

    @Nullable
    public T merge(@NotNull String userId, @NotNull T t);

    @NotNull
    public Map<String, T> findAll(@NotNull String userId);

    @Nullable
    public T persist(@NotNull String userId, @NotNull T t);

    @Nullable
    public T remove(@NotNull String userId, @NotNull String id);

    public void removeAll(@NotNull String userId);

    public void removeAll();

}
