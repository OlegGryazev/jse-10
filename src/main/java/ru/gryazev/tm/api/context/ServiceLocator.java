package ru.gryazev.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.service.*;

public interface ServiceLocator {

    @NotNull
    public IProjectService getProjectService();

    @NotNull
    public ITaskService getTaskService();

    @NotNull
    public IUserService getUserService();

    @NotNull
    public ITerminalService getTerminalService();

    @NotNull
    public ISettingService getSettingService();

}
