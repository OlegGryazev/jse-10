package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.entity.Task;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entities.values().stream().filter(o ->
                o.getProjectId() != null && o.getProjectId().equals(projectId) && userId.equals(o.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public @NotNull List<Task> findUnlinked(@NotNull String userId) {
        return findAll(userId).values().stream().filter(o ->
                o.getProjectId() == null).collect(Collectors.toList());
    }

    @Nullable
    @Override
    public Task findTaskByIndex(@NotNull String projectId, @NotNull String userId, int taskIndex) {
        return findTasksByProjectId(userId, projectId).stream().skip(taskIndex).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Task findUnlinkedTaskByIndex(@NotNull String userId, int taskIndex) {
        return findUnlinked(userId).stream().skip(taskIndex).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Task findTaskByIndex(@NotNull final String projectId,
                                @NotNull final String userId,
                                final int taskIndex,
                                @NotNull final Comparator<ComparableEntity> comparator) {
        return findTasksByProjectId(userId, projectId).stream()
                .sorted(comparator).skip(taskIndex).findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findByName(@NotNull final String userId, @NotNull final String taskName) {
        return entities.values().stream().filter(o ->
                userId.equals(o.getUserId()) && o.getName() != null && o.getName().contains(taskName))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Task> findByDetails(@NotNull final String userId, @NotNull final String taskDetails) {
        return entities.values().stream().filter(o ->
                userId.equals(o.getUserId()) && o.getDetails() != null && o.getDetails().contains(taskDetails))
                .collect(Collectors.toList());
    }

}
