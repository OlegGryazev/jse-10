package ru.gryazev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.CommandLocator;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.repository.ISettingRepository;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.*;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.command.data.AbstractDataCommand;
import ru.gryazev.tm.command.project.AbstractProjectCommand;
import ru.gryazev.tm.command.task.AbstractTaskCommand;
import ru.gryazev.tm.entity.Setting;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.error.CrudException;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.SettingRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.repository.UserRepository;
import ru.gryazev.tm.service.*;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class Bootstrap implements ServiceLocator, CommandLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISettingRepository settingRepository = new SettingRepository();

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final ISettingService settingService = new SettingService(settingRepository);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init(@NotNull final Set<Class<? extends AbstractCommand>> commandClasses) {
        terminalService.print("**** Welcome to Project Manager ****");
        settingService.setSetting(new Setting("dataDir", System.getProperty("user.dir") + "/data/"));
        commandsInit(commandClasses);
        usersInit();
        while (true) {
            try {
                @Nullable final AbstractCommand command = getCommand();
                if (!checkCommandAllowed(command)) continue;
                command.execute();
            } catch (CrudException | ClassNotFoundException | JAXBException | IOException e) {
                terminalService.print(e.getMessage());
            }
        }
    }

    private void commandsInit(@NotNull final Set<Class<? extends AbstractCommand>> commandClasses) {
        for (@NotNull final Class<? extends AbstractCommand> clazz : commandClasses){
            if (clazz == AbstractProjectCommand.class ||
                    clazz == AbstractTaskCommand.class ||
                    clazz == AbstractDataCommand.class) continue;
            @Nullable AbstractCommand command = null;
            try {
                command = clazz.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            if (command == null) continue;
            command.setServiceLocator(this);
            command.setTerminalService(terminalService);
            command.setSettingService(settingService);
            command.setCommandLocator(this);
            commands.put(command.getName(), command);
        }
    }

    private void usersInit() {
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
        user.setRoleType(RoleType.USER);
        userService.create(user.getId(), user);

        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
        admin.setRoleType(RoleType.ADMIN);
        userService.create(admin.getId(), admin);
    }

    @Nullable
    private AbstractCommand getCommand() throws IOException {
        @Nullable final AbstractCommand command = commands.get(terminalService.readCommand());
        if (command == null) {
            terminalService.print("Command not found!");
            return null;
        }
        return command;
    }

    private boolean checkCommandAllowed(AbstractCommand command) {
        if (command == null) return false;
        @Nullable final String currentUserId = settingService.findValueByKey("userId");
        @Nullable final RoleType[] roles = command.getRoles();
        final boolean checkAccess = currentUserId != null || command.isAllowed();
        final boolean checkRole = command.getRoles() == null || userService.checkRole(currentUserId, roles);
        if (!checkAccess || !checkRole) {
            terminalService.print("Access denied!");
            return false;
        }
        return true;
    }

    @Override
    public @NotNull Map<String, AbstractCommand> getCommands() {
        return commands;
    }

}