package ru.gryazev.tm.enumerated;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.Nullable;

@AllArgsConstructor
public enum Status {

    PLANNED("Planned"),
    PROCESSED("In the process"),
    COMPLETED("Completed");

    @Nullable
    private final String name;

    @Nullable
    public String displayName(){
        return name;
    }

}
